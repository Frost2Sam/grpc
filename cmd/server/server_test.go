package main

import (
	context "context"
	"log"
	"net"
	"testing"
	"time"

	pb "codeberg.org/frost2sam/grpc/pkg/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/test/bufconn"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

const (
	bufSize = 1024 * 1024
)

var listener *bufconn.Listener

func initGRPCServerBuffConn() {
	listener = bufconn.Listen(bufSize)
	s := grpc.NewServer()
	pb.RegisterCatalogServiceServer(s, &Server{})
	reflection.Register(s)
	go func() {
		if err := s.Serve(listener); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()
}
func getBufDialer(listener *bufconn.Listener) func(context.Context, string) (net.Conn, error) {
	return func(ctx context.Context, url string) (net.Conn, error) {
		return listener.Dial()
	}
}

func TestEcho(t *testing.T) {
	ctx := context.Background()
	initGRPCServerBuffConn()

	conn, err := grpc.DialContext(ctx, "bufnet",
		grpc.WithContextDialer(getBufDialer(listener)), grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := pb.NewCatalogServiceClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	resp, err := c.Echo(ctx, wrapperspb.String("ping"))
	if err != nil {
		log.Fatalf("did not invoke remote method Echo: %v", err)
	}

	r := resp.GetValue()
	if r != "pong" {
		t.Errorf("wrong answer %s instead of pong", r)
	}
}

// func BenchmarkEcho(b *testing.B) {
// 	for i := 0; i < b.N; i++ {
// 		//
// 	}
// }
