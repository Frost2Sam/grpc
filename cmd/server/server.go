package main

import (
	context "context"
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"runtime"
	"strings"
	"time"

	. "codeberg.org/frost2sam/grpc/pkg/proto"
	"github.com/gofrs/uuid"
	"google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	_ "google.golang.org/grpc/encoding/gzip"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

type Server struct {
	EntityMap map[string]*Entity
	UnimplementedCatalogServiceServer
}

func (s *Server) AddEntity(ctx context.Context, in *Entity) (*ID, error) {
	out, err := uuid.NewV4()
	if err != nil {
		return nil, status.Errorf(codes.Internal,
			"Error while generating Entity ID, %v", err)
	}
	in.Id = out.String()
	if s.EntityMap == nil {
		s.EntityMap = make(map[string]*Entity)
	}
	s.EntityMap[in.Id] = in
	return &ID{Value: in.Id}, status.New(codes.OK, "").Err()
}
func (s *Server) GetEntity(ctx context.Context, in *ID) (*Entity, error) {
	if s.EntityMap == nil {
		s.EntityMap = make(map[string]*Entity)
	}
	value, exists := s.EntityMap[in.Value]
	if exists {
		return value, status.New(codes.OK, "").Err()
	}
	return nil, status.Errorf(codes.NotFound, "entity for ID %s does not exist.", in.Value)
}
func (s *Server) DeleteEntity(ctx context.Context, in *ID) (*emptypb.Empty, error) {
	_, exists := s.EntityMap[in.Value]
	if exists {
		delete(s.EntityMap, in.Value)
	}
	return &emptypb.Empty{}, status.New(codes.OK, "").Err()
}
func (s *Server) GetAll(_ *emptypb.Empty, stream CatalogService_GetAllServer) error {
	if s.EntityMap == nil {
		s.EntityMap = make(map[string]*Entity)
	}
	if len(s.EntityMap) == 0 {
		return status.Errorf(codes.NotFound, "entities do not exist.")
	}
	for _, e := range s.EntityMap {
		err := stream.Send(e)
		if err != nil {
			return fmt.Errorf("error sending message to stream : %v", err)
		}
	}
	return status.New(codes.OK, "").Err()
}
func (s *Server) Echo(ctx context.Context, in *wrapperspb.StringValue) (*wrapperspb.StringValue, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		log.Printf("metadata from request : , %v:", md)
	}
	var out = in
	if in.GetValue() == "ping" {
		out = wrapperspb.String("pong")
	}
	mdOutHeader := metadata.Pairs("foo", "bar")
	err := grpc.SendHeader(ctx, mdOutHeader)
	if err != nil {
		log.Printf("metadata header not send : , %v:", err)
	}
	mdOutTrailer := metadata.Pairs("foo", "baz")
	err = grpc.SetTrailer(ctx, mdOutTrailer)
	if err != nil {
		log.Printf("metadata trailer not set : , %v:", err)
	}

	return out, status.New(codes.OK, "").Err()
}

type wrappedStream struct {
	grpc.ServerStream
}

func (w *wrappedStream) RecvMsg(m interface{}) error {
	log.Printf("====== [Server Stream Interceptor Wrapper] "+
		"Receive a message (Type: %T) at %s", m, time.Now().Format(time.RFC3339))
	return w.ServerStream.RecvMsg(m)
}
func (w *wrappedStream) SendMsg(m interface{}) error {
	log.Printf("====== [Server Stream Interceptor Wrapper] "+
		"Send a message (Type: %T) at %v", m, time.Now().Format(time.RFC3339))
	if w.ServerStream.Context().Err() == context.Canceled {
		return fmt.Errorf("canceled by client")
	}
	if w.ServerStream.Context().Err() == context.DeadlineExceeded {
		return fmt.Errorf("deadline exceeded")
	}
	return w.ServerStream.SendMsg(m)
}

func newWrappedStream(s grpc.ServerStream) grpc.ServerStream {
	return &wrappedStream{s}
}

func CatalogStreamServerInterceptor(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	log.Printf("====== [Server Stream Interceptor] %s\n", info.FullMethod)
	err := handler(srv, newWrappedStream(ss))
	if err != nil {
		log.Printf("RPC failed with error %v", err)
	}
	return err
}

func CatalogUnaryServerInterceptor(ctx context.Context, req interface{},
	info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, status.Errorf(codes.InvalidArgument, "missing metadata")
	}
	if !valid(md["authorization"]) {
		return nil, status.Errorf(codes.Unauthenticated, "invalid token")
	}
	log.Printf("======= [Server Interceptor] %s\n", info.FullMethod)
	m, err := handler(ctx, req)
	log.Printf(" Post Proc Message : %s", m)
	return m, err
}

func main() {
	fmt.Printf("GOMAXPROCS is %d\n", runtime.GOMAXPROCS(0))
	cert, err := tls.LoadX509KeyPair("cert/server.crt", "cert/server.key")
	if err != nil {
		log.Fatalf("failed to load key pair: %s", err)
	}
	opts := []grpc.ServerOption{
		grpc.Creds(credentials.NewServerTLSFromCert(&cert)),
		grpc.StreamInterceptor(CatalogStreamServerInterceptor),
		grpc.UnaryInterceptor(CatalogUnaryServerInterceptor),
	}
	s := grpc.NewServer(opts...)
	reflection.Register(s)
	RegisterCatalogServiceServer(s, &Server{})

	l, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("failed listening: %s", err)
	}

	log.Println("start grpc server...")
	if err := s.Serve(l); err != nil {
		log.Fatalf("server stoped: %s", err)
	}
}

func valid(authorization []string) bool {
	if len(authorization) < 1 {
		return false
	}
	token := strings.TrimPrefix(authorization[0], "Bearer ")
	return token == "some string written by William Shakespeare"
}
