package main

import (
	"context"
	"log"
	"time"

	. "codeberg.org/frost2sam/grpc/pkg/proto"
	"golang.org/x/oauth2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/oauth"
	"google.golang.org/grpc/encoding/gzip"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

func main() {
	auth := oauth.NewOauthAccess(fetchToken())

	creds, err := credentials.NewClientTLSFromFile("cert/server.crt", "go.ips.su")
	if err != nil {
		log.Fatalf("failed to load credentials: %v", err)
	}
	opts := []grpc.DialOption{
		grpc.WithPerRPCCredentials(auth),
		grpc.WithTransportCredentials(creds),
	}
	conn, err := grpc.Dial("go.ips.su:50051", opts...)
	if err != nil {
		log.Fatalf("failed dial: %s", err)
	}
	defer conn.Close()

	client := NewCatalogServiceClient(conn)
	clientDeadline := time.Now().Add(time.Duration(2 * time.Second))

	ctx, cancel := context.WithDeadline(context.Background(), clientDeadline)
	defer cancel()

	ctx = metadata.AppendToOutgoingContext(ctx,
		"timestamp", time.Now().Format(time.StampNano),
		"foo", "baz")

	var header, trailer metadata.MD
	res, err := client.Echo(ctx, wrapperspb.String("HipHop"),
		grpc.UseCompressor(gzip.Name),
		grpc.Header(&header), grpc.Trailer(&trailer))
	if err != nil {
		s := status.Code(err)
		log.Printf("Error Occured -> Echo : , %v:", s)
	}
	log.Printf("metadata from header : , %v:", header)
	log.Printf("metadata from trailer : , %v:", trailer)
	log.Printf("Response from server: %s", res.GetValue())

	res, err = client.Echo(ctx, wrapperspb.String("ping"),
		grpc.UseCompressor(gzip.Name),
		grpc.Header(&header), grpc.Trailer(&trailer))
	if err != nil {
		s := status.Code(err)
		log.Printf("Error Occured -> Echo : , %v:", s)
	}
	log.Printf("metadata from header : , %v:", header)
	log.Printf("metadata from trailer : , %v:", trailer)
	log.Printf("Response from server: %s", res.GetValue())
}

func fetchToken() *oauth2.Token {
	return &oauth2.Token{
		AccessToken: "some string written by William Shakespeare",
	}
}
