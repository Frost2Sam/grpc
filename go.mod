module codeberg.org/frost2sam/grpc

go 1.16

require (
	github.com/gofrs/uuid v4.0.0+incompatible
	golang.org/x/oauth2 v0.0.0-20210805134026-6f1e6394065a
	google.golang.org/grpc v1.39.1
	google.golang.org/protobuf v1.27.1
)
